function isZhLanguage() {
    return (navigator.language || navigator.browserLanguage).indexOf("zh-CN") > -1
}
function resetSearch(e) {
    var i = searchList[e].img;
    $("#currentSearchLogo").attr("src", i)
}
function initData() {
    var e = null
      , i = "google";
    -1 === window.location.href.indexOf("?") && (isZhLanguage() ? (i = "baidu",
    e = {
        status: 200,
        desc: "OK",
        data: [{
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/837f8d4f276d80bc98900cc6b2e7e2fa.png",
            imageType: "image",
            name: "36氪",
            url: "http://www.36kr.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/icon/150127101104.png",
            imageType: "image",
            name: "糗事百科",
            url: "http://www.qiushibaike.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/1a3448e0c67ffa2534d28dbd0d6e828c.png",
            imageType: "image",
            name: "果壳网",
            url: "http://www.guokr.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/5e4c2044072b46a7e4fbcbe829b2d021.png",
            imageType: "image",
            name: "网易新闻",
            url: "http://www.163.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/eb306ae2b122e7dde6e87fdf2970b17e.png",
            imageType: "image",
            name: "GitHub",
            url: "https://github.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/6e5da7bc6eaf3ecce1ca1affcf20fca5.png",
            imageType: "image",
            name: "慕课网",
            url: "http://www.imooc.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/a0137cb8b628250f276e6b8b192f2014.png",
            imageType: "image",
            name: "简书",
            url: "http://www.jianshu.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/89cae05550bf8320859ace31c81a8012.png",
            imageType: "image",
            name: "ZEALER",
            url: "http://www.zealer.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/d0c246241e5c5e4af2fc18bf6a86cb70.png",
            imageType: "image",
            name: "网易慕课",
            url: "http://open.163.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/82083b3712985e0aded08d10e50cb902.png",
            imageType: "image",
            name: "SMZDM",
            url: "http://www.smzdm.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/610959ecfad2fb3368d13aa1c25e3ec3.png",
            imageType: "image",
            name: "网易音乐",
            url: "http://music.163.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/0f2ab700f8fff5b6e9ebc7d6a976981f.png",
            imageType: "image",
            name: "新浪微博",
            url: "http://weibo.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/2b89ebe968d8cafe77a5c587daa79c7f.png",
            imageType: "image",
            name: "知乎",
            url: "https://www.zhihu.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/e83e603c576bc666030c3f046e5e5a0f.png",
            imageType: "image",
            name: "最美应用",
            url: "http://zuimeia.com/?utm_source=wwwd&utm_campaign=referral&utm_medium=wwwd"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/d8b62f4d64bda8800b1c788cd5ba3c68.png",
            imageType: "image",
            name: "哔哩哔哩",
            url: "http://www.bilibili.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/346647fb95fbac4d303c93fa0a4936d3.png",
            imageType: "image",
            name: "爱淘宝",
            url: "https://ai.taobao.com/?pid=mm_50570328_39070332_145428725"
        }, {
            name: "少数派",
            url: "https://sspai.com/?utm_source=infinitynewtab",
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/ab397474a68cae01968ea26324f239c2.png",
            imageType: "image"
        }, {
            name: "网易严选",
            url: "https://c.duomai.com/track.php?k=TPklWYmkTMwkDNx0DZp9VZ0l2cmYSe0lmbpZmbp1DZpVXZmcDOwM&t=http%3A%2F%2Fyou.163.com%2F",
            src: "https://infinityicon.infinitynewtab.com/icon/c4ba1648d4014bad7fdea130cd421589.png",
            imageType: "image"
        }, {
            name: "携程网",
            url: "http://u.ctrip.com/union/CtripRedirect.aspx?TypeID=2&AllianceID=12424&sid=349153&ouid=&app=0101F00&jumpUrl=http://www.ctrip.com",
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/f6ec2e6ee20fe198f81cf620413bc35b.png",
            imageType: "image"
        }, {
            name: "京东商城",
            url: "http://union.click.jd.com/jdc?e=&p=AiIBZRprFDJWWA1FBCVbV0IUEEULRFRBSkAOClBMW0srKUlmRn0nU1h3dRUPCn8MSEdwcRVZDRkOIgZlHl8XBRA3ZXopJQ%3D%3D&t=W1dCFBBFC0RUQUpADgpQTFtL",
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/cee009549b352def723ba09d6da4b742.png",
            imageType: "image"
        }],
        backupTime: 1515556313462
    }) : (i = "google",
    e = {
        status: 200,
        desc: "OK",
        data: [{
            src: "https://infinityicon.infinitynewtab.com/usericon/2ca84512c35a9aad05abee27e481dcc7.png",
            imageType: "image",
            name: "9gag",
            url: "https://9gag.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/icon/150202041300.png",
            imageType: "image",
            name: "CNN",
            url: "http://www.cnn.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/0ff885c9c58928965f6647b7e09b2be5.png",
            imageType: "image",
            name: "LinkedIn",
            url: "https://www.linkedin.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/54d04c4b9271aab96f20ec923bc16af4.png",
            imageType: "image",
            name: "reddit",
            url: "http://www.reddit.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/bc292c9e8955386cf63078c1417987be.png",
            imageType: "image",
            name: "Twitch",
            url: "http://www.twitch.tv/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/usericon/640defa5df31c8735c095f21006abc61.png",
            imageType: "image",
            name: "Tumblr",
            url: "http://tumblr.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/eb306ae2b122e7dde6e87fdf2970b17e.png",
            imageType: "image",
            name: "GitHub",
            url: "https://github.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/icon/150127093058.png",
            imageType: "image",
            name: "Spotify",
            url: "https://www.spotify.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/77e89ce307b5db74d50f6b020be8bea3.png",
            imageType: "image",
            name: "Whatsapp",
            url: "https://web.whatsapp.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/40ccf61bdd88242bed21836b83b8c65b.png",
            imageType: "image",
            name: "Pinterest",
            url: "https://www.pinterest.com/"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/f3f51cfeedd6ee48f84b99c6347574e0.png",
            imageType: "image",
            name: "Facebook Messenger",
            url: "https://m.facebook.com/messages"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/a6d0e807a4a514867dd675cbc63890ed.png",
            imageType: "image",
            name: "Instagram",
            url: "http://www.instagram.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/e67eed044bf08fbcac16a0527fcc165a.png",
            imageType: "image",
            name: "Amazon",
            url: "https://amazon.com"
        }, {
            src: "https://infinityicon.infinitynewtab.com/usericon/07ec46eac62dca559954f3c21736b5c0.png",
            imageType: "image",
            name: "AliExpress",
            url: "http://s.click.aliexpress.com/e/VVJmaUj"
        }, {
            name: "Apple",
            url: "https://apple.com",
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/ad1a667cc2ea0abc8bd54a0782fd6407.png",
            imageType: "image"
        }, {
            name: "Youtube",
            url: "https://youtube.com",
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/37d396f9975e494b10ac8696d64ebb2a.png",
            imageType: "image"
        }, {
            name: "Twitter",
            url: "https://twitter.com",
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/0c7e5d8b40c38cde576595b23546cc91.png",
            imageType: "image"
        }, {
            name: "Booking",
            url: "https://www.booking.com/index.html?aid=1267011",
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/83e58c13ed40dc8393297d43d2639cce.png",
            imageType: "image"
        }, {
            name: "eBay",
            url: "http://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_ff3=1&pub=5575304691&toolid=10001&campid=5338095340&customid=&ipn=psmain&icep_vectorid=229466&kwid=902099&mtid=824&kw=lg",
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/c11671f9b92cd56a5ef44aa0ea36e099.png",
            imageType: "image"
        }, {
            name: "500px",
            url: "https://500px.com",
            src: "https://infinityicon.infinitynewtab.com/user-share-icon/ee908304d3c7def64adc6b3e2aaac4df.png",
            imageType: "image"
        }],
        backupTime: 1515571362477
    }));
    var n = getStorage("defaultData");
    n || (void 0,
    e ? dellWithShowIconData(e) : (void 0,
    getNewData())),
    n && dellWithShowIconData(n);
    var t = getStorage("defaultSearchType");
    t || (resetSearch(i),
    setStorage("defaultSearchType", i)),
    t && resetSearch(t)
}
function reloadPageInit() {
    var e = $("#ul-box");
    "none" == e.css("display") && e.show(),
    0 !== $("#search-suggestion ul").length && $("#search-suggestion ul").hide()
}
function getParameters(e, i) {
    i || (i = location.href);
    var n = "[\\?&]" + (e = e.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]")) + "=([^&#]*)"
      , t = new RegExp(n).exec(i);
    return null == t ? null : t[1]
}
function getNewData() {
    var e = window.location.href
      , i = ""
      , n = getParameters("uid", e)
      , t = getParameters("secret", e);
    if (!n && !t) {
        var a = e.split("?");
        if (2 !== a.length || 6 !== a[1].length)
            return;
        i = "https://infinity-api.infinitynewtab.com/c/" + a[1]
    }
    n && t && (i = "https://infinity-api.infinitynewtab.com/c/0?uid=" + n + "&secret=" + t),
    $.get(i, function(e, i, n) {
        if ("success" == i && 200 === e.status) {
            var t = e.backupTime
              , a = getStorage("defaultData");
            a ? a.backupTime !== t && (dellWithShowIconData(e),
            setStorage("defaultData", e)) : (dellWithShowIconData(e),
            setStorage("defaultData", e))
        }
    })
}
function getFolderUrl(e) {
    var i = window.location.href
      , n = getParameters("uid", i)
      , t = getParameters("secret", i)
      , a = null;
    if (!n || !t) {
        var o = i.split("?");
        2 === o.length && 6 === o[1].length && (a = o[1])
    }
    var c = location.protocol + "//" + location.host + "/folder.html?"
      , s = ["folderId=" + e];
    return n && t && s.push("uid=" + n + "&secret=" + t),
    a && s.push("mobileId=" + a),
    c + s.join("&")
}
function dellWithShowIconData(e) {
    for (var i = "", n = (e = e.data).length, t = n; n--; ) {
        var a = e[n].url
          , o = e[n].name
          , c = e[n].src;
        switch (e[n].imageType) {
        case "color":
            i += '<li><a href="' + a + '"target="_self"><div class="icon-img-box" style="background:' + e[n].bgColor + ';" >' + o + '</div><p class="overflow-ellips">' + o + "</p></a></li>";
            break;
        case "folder":
            i += '<li><a href="' + (a = getFolderUrl(e[n].id)) + '"target="_self"><div class="icon-img-box folder-icon-img-box"><img src="./img/folder.png"></div><p class="overflow-ellips">' + o + "</p></a></li>";
            break;
        default:
            i += '<li><a href="' + a + '"target="_self"><div class="icon-img-box"><img src="' + c + '"></div><p class="overflow-ellips">' + o + "</p></a></li>"
        }
    }
    i += '<li id="fakeitem"></li>',
    document.getElementById("list-item").innerHTML = i,
    calcateIconLayOut(t),
    getNewData()
}
function calcateIconLayOut(e) {
    var i = elementResizeDetectorMaker()
      , n = (elementResizeDetectorMaker({
        strategy: "scroll"
    }),
    e);
    void 0;
    var t = document.getElementById("ul-box")
      , a = document.getElementById("list-item")
      , o = document.getElementById("fakeitem");
    void 0;
    (document.body.clientWidth / 100).toFixed(2);
    void 0,
    layloutReset(n, a, o, !1),
    i.listenTo(t, function(e) {
        layloutReset(n, a, o, !1)
    })
}
function layloutReset(e, i, n, t) {
    var a = i.offsetWidth
      , o = document.body.clientWidth / 100 * 2.4.toFixed(3)
      , c = 50 + 2 * o;
    void 0;
    var s = Math.floor(a / c);
    void 0;
    var r = ((a - s * c) / (s - 1)).toFixed(3);
    void 0;
    var m = e % s;
    if (void 0,
    m > 0) {
        var g = s - m;
        void 0;
        var l = (g * c + (g - 1) * r).toFixed(2);
        void 0,
        n.style.cssText = "display:block;visibility: hidden;height: 0;padding:0;margin:0;width:" + l + "px;"
    } else
        void 0,
        n.style.cssText = "display:none;"
}
function handleOutboundLinkClicks(e, i) {
    gtag("event", "search", {
        event_action: e,
        event_label: i
    })
}
function openSearchResult(e) {
    var i = getStorage("defaultSearchType")
      , n = searchList[i].url + e;
    handleOutboundLinkClicks(i, e),
    0 !== $("#search-suggestion ul").length && ($("#search-suggestion ul").hide(),
    $("#ul-box").show()),
    window.location.href = n
}
function Arrayprocess(e) {
    for (var i = [], n = 0, t = e.length; n < t; n++)
        i.push(e[n].word);
    return i
}
function isSuportLocalStorage() {
    return !!window.localStorage
}
function setStorage(e, i) {
    var n = Object.prototype.toString.call(i);
    "[object Array]" !== n && "[object Object]" !== n || (i = JSON.stringify(i)),
    localStorage.setItem(e, i),
    !localStorage[i] && i.length < 50 && Cookies.set(e, i, {
        expires: 37
    })
}
function getStorage(e) {
    try {
        var i = localStorage.getItem(e);
        return i ? JSON.parse(i) : Cookies.get(e)
    } catch (e) {
        return i
    }
}
function temp(e, i) {
    var n = document.createElement("div");
    n.setAttribute("style", "position:fixed;z-index:999;top:0;background-color:#fff;width:100%;left:0;text-align:center;height:40px;line-height:40px;color:#a5a5a5; "),
    n.innerHTML = e,
    setTimeout(function() {
        n.parentNode.removeChild(n)
    }, i),
    document.body.appendChild(n)
}
var searchList = {
    google: {
        url: "https://www.google.com/search?q=",
        img: "./img/google.png"
    },
    yahoo: {
        url: "https://search.yahoo.com/search?isource=infinity&iname=yahoo&itype=web&p=",
        img: "./img/yahoo.png"
    },
    baidu: {
        url: "https://www.baidu.com/s?wd=",
        img: "./img/baidu.png"
    },
    bing: {
        url: "https://bing.com/search?isource=infinity&iname=bing&itype=web&q=",
        img: "./img/bing.png"
    },
    yandex: {
        url: "https://yandex.com/search/?isource=infinity&itype=web&iname=yandex&text=",
        img: "./img/yandex.png"
    },
    duckduckgo: {
        url: "https://duckduckgo.com/?isource=infinity&iname=duckduckgo&itype=web&q=",
        img: "./img/duckduckgo.png"
    },
    360: {
        url: "https://www.so.com/s?src=lm&ls=sm2054017&lm_extend=ctype:4&q=",
        img: "./img/360.png"
    },
    sogou: {
        url: "https://sogou.com/web?isource=infinity&iname=sogou&itype=web&_asf=infinitynewtab.com&query=",
        img: "./img/sogou.png"
    }
}
  , i18n = {
    get: function(e) {
        return isZhLanguage() ? this[e].zh : this[e].en
    },
    Copysuccessfully: {
        zh: "复制成功",
        en: "Copy successfully"
    },
    Copyfailed: {
        zh: "复制失败",
        en: "Copy failed"
    },
    Addtomainscreen: {
        zh: "添加到主屏幕",
        en: "Add to main screen"
    },
    Clicktoshare: {
        zh: "点击分享",
        en: "Click to share"
    },
    Safariopens: {
        zh: "Safari浏览器打开",
        en: "Safari opens"
    },
    pleaseuse: {
        zh: "请使用",
        en: "please use"
    },
    Clickcopy: {
        zh: "点击复制",
        en: "Click copy"
    },
    Addtothedesktop: {
        zh: "添加到桌面",
        en: "Add to the desktop"
    },
    donotshowagain: {
        zh: "不再显示",
        en: "do not show again"
    }
};
try {
    initData()
} catch (e) {
    console(e)
}
var browser = {
    versions: function() {
        var e = navigator.userAgent;
        navigator.appVersion;
        return {
            trident: e.indexOf("Trident") > -1,
            presto: e.indexOf("Presto") > -1,
            webKit: e.indexOf("AppleWebKit") > -1,
            gecko: e.indexOf("Gecko") > -1 && -1 == e.indexOf("KHTML"),
            mobile: !!e.match(/AppleWebKit.*Mobile.*/),
            ios: !!e.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
            android: e.indexOf("Android") > -1 || e.indexOf("Adr") > -1,
            iPhone: e.indexOf("iPhone") > -1,
            iPad: e.indexOf("iPad") > -1,
            webApp: -1 == e.indexOf("Safari"),
            weixin: e.indexOf("MicroMessenger") > -1,
            qq: null !== e.match(/\sQQ/i),
            Safari: e.indexOf("Safari") > -1 && e.indexOf("Chrome") < 1 && e.indexOf("Version") > -1
        }
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
};
$(function() {
    if (browser.versions.mobile) {
        FastClick.attach(document.body);
        var e = "";
        if (void 0,
        browser.versions.ios)
            if (browser.versions.Safari) {
                if ("y" === getStorage("isShowRemind"))
                    return;
                e += '<div id="remind-to-home"><div class="remind-to-home-box"><img src="img/icon-add-to-screen.png" ><span id="add-to-screen">' + i18n.get("Addtothedesktop") + '</span><button id="no-show">' + i18n.get("donotshowagain") + '</button></div><div class="triangle-down"></div> </div>',
                $("#remindHomeTopBox").html(e)
            } else if (browser.versions.weixin || browser.versions.qq)
                e += '<div id="remindBrower">                    <div id="arrowsWeixin">                    <img src="img/icon-arrows-right.svg" >                    </div>                    <div id="remindFlow">                <div class="remind-flow1">                    <img src="img/icon-safari.svg" >                    <span>                    <p class="remind-font firstp">' + i18n.get("pleaseuse") + '</p>                    <p class="remind-font">' + i18n.get("Safariopens") + '</p>                </span>                </div>                <div class="renmind-arrows">                    <img src="img/icon-next.svg" >                </div>                <div>                    <img src="img/icon-Safari – Share.svg" >                    <p class="remind-font remind-title">' + i18n.get("Clicktoshare") + '</p>                </div>                <div class="renmind-arrows">                    <img src="img/icon-next.svg" >                </div>                <div>                    <img src="img/icon-safari-add-homescreen.svg" >                    <p class="remind-font remind-title">' + i18n.get("Addtomainscreen") + "</p>                </div>                 </div>                </div>",
                $("#remindBrowerTopBox").html(e);
            else {
                if ("y" === getStorage("isremind"))
                    return;
                e += '<div id="remindBrower">                    <div id="arrowsWeixin">                    </div>                    <div id="remindFlow">                    <div class="remind-flow1">                    <img src="img/icon-safari.svg" >                    <span>                    <p class="remind-font firstp">' + i18n.get("pleaseuse") + '</p>                    <p class="remind-font">' + i18n.get("Safariopens") + '</p>                    </span>                    </div>                    <div class="renmind-arrows">                    <img src="img/icon-next.svg" >                    </div>                    <div>                    <img src="img/icon-Safari – Share.svg" >                    <p class="remind-font remind-title">' + i18n.get("Clicktoshare") + '</p>                    </div>                    <div class="renmind-arrows">                    <img src="img/icon-next.svg" >                    </div>                    <div class="close-button-remind"></div>                <div>                    <img src="img/icon-safari-add-homescreen.svg" >                    <p class="remind-font remind-title">' + i18n.get("Addtomainscreen") + '</p>                </div>            </div>            <div id="content-url">                <div class="remind-font1" id="UrlCopy">' + window.location.href + '</div>                <button class="remind-font1" id="copyUrlBtn" data-clipboard-target="#UrlCopy">' + i18n.get("Clickcopy") + "</button>            </div>        </div>",
                $("#remindBrowerTopBox").html(e)
            }
        else if (browser.versions.android) {
            if ("y" === getStorage("isShowRemind"))
                return;
            e += '<div id="remind-to-home"><div class="remind-to-home-box"><img src="img/icon-add-to-screen.png" ><span id="add-to-screen">' + i18n.get("Addtothedesktop") + '</span><button id="no-show">' + i18n.get("donotshowagain") + "</button></div> </div>",
            $("#remindHomeTopBox").html(e)
        }
    }
});
var defaultSearchType = "google"
  , language = navigator.language || navigator.browserLanguage;
defaultSearchType = language.indexOf("zh") > -1 ? "baidu" : "google",
$("#searchSubmit").on("submit", function(e) {
    openSearchResult($("#searchInput").val()),
    e.preventDefault()
}),
$("#search-list").on("click", "li", function() {
    var e = $(this).data("search_type");
    $(this).children("img").attr("src");
    resetSearch(e),
    $("#search-list").hide(),
    $("#ul-box").show(),
    setStorage("defaultSearchType", e)
}),
$(".search-logo").on("click", function() {
    "none" == $("#search-list").css("display") ? ($("#search-list").show(),
    $("#ul-box").hide()) : ($("#search-list").hide(),
    $("#ul-box").show())
}),
$("#searchInput").on("input", function() {
    var e = $(this).val();
    if (0 === e.length)
        return $("#search-suggestion").html(""),
        void $("#ul-box").show();
    var i = "https://sug.so.360.cn/suggest?callback=?&encodein=utf-8&encodeout=utf-8&format=json&fields=word&word=" + e
      , n = isZhLanguage();
    n || (i = "https://suggestqueries.google.com/complete/search?output=chrome&hl=" + navigator.language + "&q=" + e + "&hl=" + navigator.language + "&infinityTime=1515986391815"),
    void 0,
    $.ajax({
        url: i,
        dataType: "jsonp",
        success: function(e) {
            var i = [];
            n || (i = e[1].slice(0, 10)),
            n && (i = Arrayprocess(e.result),
            void 0);
            var t = ""
              , a = i.length;
            if (0 != a) {
                for (t += "<ul>"; a--; )
                    t += "<li>" + i[a] + "</li>";
                void 0,
                t += "</ul>",
                $("#search-suggestion").html(t),
                $("#ul-box").hide()
            } else
                t += "</ul>",
                $("#search-suggestion").html(t),
                $("#ul-box").show()
        },
        error: function(e) {
            void 0
        }
    })
}),
$("#search-suggestion").on("click", "li", function() {
    openSearchResult($(this).text())
}),
window.addEventListener("pageshow", function(e) {
    e.persisted && reloadPageInit()
});
var matches = function(e, i) {
    return (e.matches || e.matchesSelector || e.msMatchesSelector || e.mozMatchesSelector || e.webkitMatchesSelector || e.oMatchesSelector).call(e, i)
}
  , t = matches(document.getElementById("search-list"), "#search-list");
if ($(document).on("click", function(e) {
    var i = $("#search-list");
    0 === $(event.target).closest(i).length && 0 === i.has(e.target).length && 1 !== $(".search-logo").has(event.target).length && "search-logo" != event.target.className && ("none" !== i.css("display") && i.hide(),
    "none" == $("#ul-box").css("display") && $("#ul-box").show())
}),
Clipboard.isSupported()) {
    var clipboard = new Clipboard("#copyUrlBtn");
    clipboard.on("success", function(e) {
        e.clearSelection(),
        tempAlert(i18n.get("Copysuccessfully"), 1500)
    }),
    clipboard.on("error", function(e) {
        tempAlert(i18n.get("Copyfailed"), 1500)
    })
}
$(document).on("click", ".close-button-remind", function() {
    $("#remindBrower").hide(),
    setStorage("isremind", "y")
}),
$(document).on("click", "#no-show", function() {
    setStorage("isShowRemind", "y"),
    $("#remind-to-home").remove()
});
